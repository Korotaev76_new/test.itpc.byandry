﻿using System;
using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;

using ru.itpc.trial.Models;
using ru.itpc.trial.Data;

namespace ru.itpc.trial.test.Data
{
    [TestFixture]
    public class DataContextTest
    {
        private DataContext dataContext;

        [SetUp]
        public void setUp()
        {
            this.dataContext = new StorageDataContext();

        }

        [Test]

        public void Get_LastUpdate_test()
        {

            Assert.LessOrEqual(this.dataContext.Get<DateTime>(), DateTime.Now);

        }

        [Test]
        public void Get_IEnumerable_Addresses_test()
        {

            IEnumerable<AddressRecord> addressRecords =

                this.dataContext.Get<IEnumerable<AddressRecord>>();

            IEnumerable<Address> addresses =

                this.dataContext.Get<IEnumerable<Address>>();

           // var a = addressRecords.

           

           

            Assert.AreSame(addressRecords, addresses);

            Assert.NotNull(addressRecords);

            Assert.NotNull(addresses);



            Assert.AreEqual(0, addressRecords.Count());

            Assert.AreEqual(0, addresses.Count());

        }



        [Test]

        public void Get_ICollection_of_Addresses_test()
        {

            ICollection<AddressRecord> addresses =

                this.dataContext.Get<ICollection<AddressRecord>>();

            Assert.NotNull(addresses);

            Assert.AreEqual(0, addresses.Count());

        }

        [Test]
        public void Get_IEnumerable_test()
        {
            IEnumerable<PersonRecord> personRecords =
                this.dataContext.Get<IEnumerable<PersonRecord>>();
            IEnumerable<Person> persons =
                this.dataContext.Get<IEnumerable<Person>>();

            Assert.AreSame(personRecords, persons);

            Assert.NotNull(personRecords);
            Assert.NotNull(persons);

            Assert.AreEqual(0, personRecords.Count());
            Assert.AreEqual(0, persons.Count());
        }

        [Test]
        public void Get_ICollection_test()
        {
            ICollection<PersonRecord> persons =
                this.dataContext.Get<ICollection<PersonRecord>>();

            Assert.NotNull(persons);
            Assert.AreEqual(0, persons.Count());
        }
    }
}
