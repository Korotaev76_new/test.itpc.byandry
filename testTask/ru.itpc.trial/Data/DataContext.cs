﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ru.itpc.trial.Models;

namespace ru.itpc.trial.Data
{
    public interface DataContext
    {
        T Get<T>();
    }


    public class StorageDataContext : DataContext
    {
        public List<PersonRecord> PersonRecords { get; set; }

        public List<AddressRecord> AddressRecords { get; set; }

        public List<string> Strings { get; set; }

        public List<int> Integers { get; set; }

        public DateTime LastUpdate { get; set; }


        public StorageDataContext()
        {
            PersonRecords = new List<PersonRecord>();

            AddressRecords = new List<AddressRecord>();

            Strings = new List<string>();

            Integers = new List<int>();

            this.LastUpdate = DateTime.Now;
        }


        public T Get<T>()
        {
            Type NeedType;

            var obj = typeof(T);

            if (obj.IsGenericType)
            {
                obj = typeof(T).GetGenericArguments()[0];    
            }
                        
            var a = Assembly.GetAssembly(obj).GetTypes().Where(type => type.IsSubclassOf(obj));

            var child = a.FirstOrDefault();

            if (child != null)
            {
                NeedType = child;
            }
            else
            {
                NeedType = obj;
            }

            var allProp  = GetType()
                    .GetProperties()
                    .Where(prop => prop.PropertyType == obj);

            if (typeof(T).IsGenericType)
            {
                allProp = GetType()
                    .GetProperties()
                    .Where(prop => prop.PropertyType.GetGenericArguments()[0] == NeedType);
            }
            
            var data = allProp.FirstOrDefault();
            if (data == null)
            {
                return default(T);
            }
           
            var list = (T) data.GetValue(this);
            return list;
        }
    }

    public static class DefaultDataContext
    {
        public static StorageDataContext Instance = new StorageDataContext();
    }
}