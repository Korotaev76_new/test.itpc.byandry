﻿using System;

namespace ru.itpc.trial.Models
{
    public interface IData
    {
    }

    public class PersonRecord : Person, IData
    {
        public DateTime BirthDate { get; private set; }

        public PersonRecord()
        {
        }

        public PersonRecord(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
            BirthDate = DateTime.Now;
            Id = Guid.NewGuid().ToString();
        }
    }

    public class Person : Identified<string>, IData
    {
        public string FirstName;
        public string LastName;

        public Person(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;

            Id = Guid.NewGuid().ToString();
        }

        public Person()
        {
        }
    }

    public class Identified<TObject>
    {
        public TObject Id { get; set; }

        public Identified(TObject id)
        {
            Id = id;
        }

        public Identified()
        {
        }
    }

    public class Address : Identified<string>, IData
    {
        public string City;
        public string Street;
    }

    public class AddressRecord : Address, IData
    {
        public DateTime DateCreated;

        public AddressRecord()
        {
        }

        private AddressRecord(string city, string street)
        {
            DateCreated = DateTime.Now;
            City = city;
            Street = street;
            Id = "1";
        }
    }
}